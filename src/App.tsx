import { AppRoutes } from './common/constants';

function App() {
  return (
    <div>
      <header>
        <a href={AppRoutes.HOME}>
          <h1>Awesome Posts 🤓</h1>
        </a>
      </header>
      {/**
       * Роутинг здесь
       */}
    </div>
  );
}

export default App;
